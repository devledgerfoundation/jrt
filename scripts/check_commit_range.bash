#!/usr/bin/env bash

# Example usage:
#   ./scripts/check_commit_range.bash $(git merge-base HEAD @{u})..HEAD 'npx tsc'

current_branch=$(git rev-parse --abbrev-ref HEAD)

while read -r rev; do
    git checkout "$rev" > /dev/null 2>&1
    printf "Checking commit '%s'... " "$rev"
    if ! eval $2; then
        >&2 echo "[FAIL]"
        exit 1
    fi
    echo "[OK]"
done < <(git rev-list --reverse "$1")

git checkout "$current_branch" > /dev/null
