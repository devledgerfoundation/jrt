pragma solidity ^0.5.7;

import 'openzeppelin-eth/contracts/token/ERC20/ERC20.sol';

contract AToken is ERC20 {
  function allowInterestRedirectionTo(address _to) external;

  function burnOnLiquidation(address _account, uint256 _value) external;

  function getInterestRedirectionAddress(address _user)
    external
    view
    returns (address);

  function getRedirectedBalance(address _user) external view returns (uint256);

  function getUserIndex(address _user) external view returns (uint256);

  function isTransferAllowed(address _user, uint256 _amount)
    external
    view
    returns (bool);

  function mintOnDeposit(address _account, uint256 _amount) external;

  function principalBalanceOf(address _user) external view returns (uint256);

  function redeem(uint256 _amount) external;

  function redirectInterestStream(address _to) external;

  function redirectInterestStreamOf(address _from, address _to) external;

  function transferOnLiquidation(
    address _from,
    address _to,
    uint256 _value
  ) external;

  function underlyingAssetAddress() external view returns (address);

  event Redeem(
    address indexed _from,
    uint256 _value,
    uint256 _fromBalanceIncrease,
    uint256 _fromIndex
  );

  /**
   * @dev emitted after the mint action
   * @param _from the address performing the mint
   * @param _value the amount to be minted
   * @param _fromBalanceIncrease the cumulated balance since the last update of the user
   * @param _fromIndex the last index of the user
   **/
  event MintOnDeposit(
    address indexed _from,
    uint256 _value,
    uint256 _fromBalanceIncrease,
    uint256 _fromIndex
  );

  /**
   * @dev emitted during the liquidation action, when the liquidator reclaims the underlying
   * asset
   * @param _from the address from which the tokens are being burned
   * @param _value the amount to be burned
   * @param _fromBalanceIncrease the cumulated balance since the last update of the user
   * @param _fromIndex the last index of the user
   **/
  event BurnOnLiquidation(
    address indexed _from,
    uint256 _value,
    uint256 _fromBalanceIncrease,
    uint256 _fromIndex
  );

  /**
   * @dev emitted during the transfer action
   * @param _from the address from which the tokens are being transferred
   * @param _to the adress of the destination
   * @param _value the amount to be minted
   * @param _fromBalanceIncrease the cumulated balance since the last update of the user
   * @param _toBalanceIncrease the cumulated balance since the last update of the destination
   * @param _fromIndex the last index of the user
   * @param _toIndex the last index of the liquidator
   **/
  event BalanceTransfer(
    address indexed _from,
    address indexed _to,
    uint256 _value,
    uint256 _fromBalanceIncrease,
    uint256 _toBalanceIncrease,
    uint256 _fromIndex,
    uint256 _toIndex
  );

  /**
   * @dev emitted when the accumulation of the interest
   * by an user is redirected to another user
   * @param _from the address from which the interest is being redirected
   * @param _to the adress of the destination
   * @param _fromBalanceIncrease the cumulated balance since the last update of the user
   * @param _fromIndex the last index of the user
   **/
  event InterestStreamRedirected(
    address indexed _from,
    address indexed _to,
    uint256 _redirectedBalance,
    uint256 _fromBalanceIncrease,
    uint256 _fromIndex
  );

  /**
   * @dev emitted when the redirected balance of an user is being updated
   * @param _targetAddress the address of which the balance is being updated
   * @param _targetBalanceIncrease the cumulated balance since the last update of the target
   * @param _targetIndex the last index of the user
   * @param _redirectedBalanceAdded the redirected balance being added
   * @param _redirectedBalanceRemoved the redirected balance being removed
   **/
  event RedirectedBalanceUpdated(
    address indexed _targetAddress,
    uint256 _targetBalanceIncrease,
    uint256 _targetIndex,
    uint256 _redirectedBalanceAdded,
    uint256 _redirectedBalanceRemoved
  );

  event InterestRedirectionAllowanceChanged(
    address indexed _from,
    address indexed _to
  );
}
