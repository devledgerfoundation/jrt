pragma solidity ^0.5.7;

import 'zos-lib/contracts/Initializable.sol';
import './ERC20.sol';
import './ERC20Burnable.sol';
import './ERC20Pausable.sol';
import './ERC20Mintable.sol';
import './Permit.sol';

contract JarvisRewardToken is
  Initializable,
  ERC20,
  ERC20Burnable,
  ERC20Pausable,
  ERC20Mintable,
  Permit
{
  string public constant name = 'Jarvis Reward Token';
  string public constant symbol = 'JRT';
  uint8 public constant decimals = 18;

  function upgradeToV2(uint256 chainId, address minter) public {
    require(!upgradedToV2, 'Contract already upgraded');
    upgradedToV2 = true;
    initializeMinterRole(minter);
    initializePermit(name, chainId);
  }

  function transferToMany(
    address[] calldata _recipients,
    uint256[] calldata _values
  ) external returns (bool) {
    require(_recipients.length == _values.length);
    for (uint256 i = 0; i < _values.length; i++) {
      require(transfer(_recipients[i], _values[i]));
    }
    return true;
  }
}
