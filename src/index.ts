import program from 'commander';

import {
  assertIsAddress,
  loadJSON,
  normailzeRecipientList,
} from './base/utils';
import { Distributor } from './distributor';
import { env } from './base/config';
import Web3 from 'web3';
import _ from 'lodash';
import { Recipient } from './base/models';

declare const artifacts: any;
declare const web3: Web3;

// truffle exec calls this function as the module entry-point:
module.exports = async function (callback: () => void) {
  try {
    await new Promise<void>((resolve, reject) => {
      handleCLI(web3, resolve, reject);
    });
  } catch (err) {
    console.log(err);
  }
  callback();
};

function handleCLI(
  web3: Web3,
  resolve: (value?: void | PromiseLike<void>) => void,
  reject: (reason?: any) => void,
) {
  program
    .command('send <recipients-file>')
    .description(
      'Distribute the tokens to recipients as specified in <recipients-file>.',
    )
    .option(
      '-c, --contract-address <address>',
      'The address where the contract is deployed',
      env.ethAddresses.jrtContract,
    )
    .option(
      '-w, --wallet-address <address>',
      'The contract owner address',
      env.ethAddresses.deployingWallet,
    )
    .option('-p, --pause', 'Pause the smart contract after distribution', false)
    .option('--no-verify', 'Skip verification process', false)
    .action(async (recipientsFile, options) => {
      try {
        const contractAddr = assertIsAddress(options.contractAddress);
        const walletAddr = assertIsAddress(options.walletAddress);
        const pause = options.pause as boolean;
        const d = await getDistributor(
          { web3 },
          contractAddr,
          walletAddr,
          recipientsFile,
        );
        const count = d.entries.length;
        console.log(`Sending transactions from '${walletAddr}'.`);
        console.log(`Distributing to '${count}' addresses.`);
        console.log(options);
        await d.unpause();
        await d.distribute(0, count, env.constants.BATCH_TRANSACTION_COUNT);
        if (pause) {
          await d.pause();
        }
        if (!program.noVerify) {
          await d.verifyBalances(0, count);
        }
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  program
    .command('verify-recipient-balances <recipients-file>')
    .alias('verify')
    .description(
      "Verify that the recipients' balances are as specified in <recipients-file>.",
    )
    .option(
      '-c, --contract-address <address>',
      'The address where the contract is deployed',
      env.ethAddresses.jrtContract,
    )
    .action(async (recipientsFile, options) => {
      try {
        const contractAddr = assertIsAddress(options.contractAddress);
        const d = await getDistributor(
          { web3 },
          contractAddr,
          null,
          recipientsFile,
        );
        console.log(`Is JRT paused: ${await d.paused()}.`);
        await d.verifyBalances(0, d.entries.length);
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  program
    .command('pause')
    .description('Pause the ERC20 smart contract.')
    .option(
      '-c, --contract-address <address>',
      'The address where the contract is deployed',
      env.ethAddresses.jrtContract,
    )
    .option(
      '-w, --wallet-address <address>',
      'The contract owner address',
      env.ethAddresses.deployingWallet,
    )
    .action(async options => {
      try {
        const d = await getDistributor(
          { web3 },
          options.contractAddress,
          options.walletAddress,
          null,
        );
        await d.pause();
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  program
    .command('unpause')
    .description('Unpause the ERC20 smart contract.')
    .option(
      '-c, --contract-address <address>',
      'The address where the contract is deployed',
      env.ethAddresses.jrtContract,
    )
    .option(
      '-w, --wallet-address <address>',
      'The contract owner address',
      env.ethAddresses.deployingWallet,
    )
    .action(async options => {
      try {
        const d = await getDistributor(
          { web3 },
          options.contractAddress,
          options.walletAddress,
          null,
        );
        await d.unpause();
        resolve();
      } catch (error) {
        reject(error);
      }
    });

  program.parse(['node'].concat(process.argv.slice(5)));
}

export async function getDistributor(
  { web3, JarvisRewardToken }: { web3: Web3; JarvisRewardToken?: any },
  contractAddress: string,
  walletAddress: string,
  recipientsFile: string,
) {
  JarvisRewardToken =
    JarvisRewardToken || artifacts.require('JarvisRewardToken');
  console.log(
    `Using contract '${JarvisRewardToken.contractName}' deployed at '${contractAddress}'.`,
  );
  if (walletAddress)
    console.log(`Sending transactions from: '${walletAddress}'.`);
  let entries = null;
  if (recipientsFile && recipientsFile.length) {
    console.log(`Loading investors from '${recipientsFile}'...`);
    const list = loadJSON(recipientsFile);
    entries = await normailzeRecipientList(list);
  }
  const jrt = await JarvisRewardToken.at(contractAddress);
  return new Distributor(web3, entries, walletAddress, jrt);
}

module.exports.getJrtDist = async function ({
  web3,
  JarvisRewardToken,
}: {
  web3: Web3;
  JarvisRewardToken: any;
}) {
  const contractAddress = '0x8A9C67fee641579dEbA04928c4BC45F66e26343A';
  const walletAddress = '0x0d54aadd7ce2dc10eb9527c6105a3c3f1b463d1b';
  const jrt = await JarvisRewardToken.at(contractAddress);
  const recipeints: Recipient[] = [];
  return new Distributor(web3, recipeints.sortByAmount(), walletAddress, jrt);
};

//module.exports = async function(callback) {
// async function old(callback) {
//   try {
//     const contractAddr = program.contractAddress;
//     const sender = program.walletAddress;
//     const entries = loadInvestorList(program.recipientsFile);
//     const count = entries.length;
//     const JarvisRewardToken = artifacts.require('JarvisRewardToken');
//     const jrt = await JarvisRewardToken.at(contractAddr);
//     const d = new Distributor(web3, entries, sender, jrt);
//     console.log(`Using contract '${JarvisRewardToken.contractName}' deployed at '${contractAddr}'.`);
//     if (!program.verifyOnly) {
//       console.log(`Sending transactions from '${sender}'.`);
//       console.log(`Distributing to '${count}' addresses.`);
//       await d.unpause();
//       await d.distribute(0, count, BATCH_TRANSACTION_COUNT);
//       await d.pause();
//     } else {
//       console.log(`Is JRT paused: ${await d.jrt.paused()}.`);
//     }
//     await d.verifyBalances(0, count);
//   } catch (error) {
//     console.error(error);
//   }
//   callback()
// };
