import BN from 'bn.js';
import Web3 from 'web3';
import {
  getAllRedirectInfo,
  TimestampedRedirectEvent,
  getAllDepositInfo,
  getAllWithdrawInfo,
  getAllTransferInfo,
} from '../base/aave';
import { getBlockTimestamp, getClosestBlock } from '../base/web3';
import type { AToken } from '../contracts/AToken';
import { assert } from 'console';
import { SortedArray } from '../base/sorting';
import { env } from '../base/config';
import { zero, mapSumBN } from '../base/utils';
import { SnapshotWeightsOnly, Snapshot, SnapshotWithName } from './snapshot';

type UserHistory = { startTime: number; endTime: number; amount: BN };

export type AggregateSnapshot = {
  startBlock: number; // the first block included in the interval (inclusive) | 05:00:10
  endBlock: number;
  startingTime: number; // the last block included in the interval (inclusive)  | 05:59:57
  endingTime: number;
  participants: {
    // mapping of address to balance for each participant
    [address: string]: {
      weight: BN;
    };
  };
};

type UserOperation = {
  type: 'Deposit' | 'Withdraw' | 'InTransfer' | 'OutTransfer';
  timestamp: number;
  amount: BN;
};

type AaveCalculateParams = {
  web3: Web3;
  Atoken: AToken;
  tokenName: string;
  previousSnapshot: Snapshot;
  startTimestamp: number;
  duration: number;
  decimals: number;
};

export async function AaveCalculate({
  web3,
  Atoken,
  tokenName,
  previousSnapshot,
  startTimestamp,
  duration,
  decimals,
}: AaveCalculateParams): Promise<SnapshotWithName> {
  const firstNewBlockTimestamp = await getBlockTimestamp(
    web3,
    previousSnapshot.endBlock + 1,
  );
  assert(
    startTimestamp >= previousSnapshot.endingTime &&
      startTimestamp <= firstNewBlockTimestamp,
    'Wrong starting time',
  );
  const endTimestamp = startTimestamp + duration;
  const toBlock = await getClosestBlock(
    web3,
    previousSnapshot.endBlock,
    endTimestamp,
  );

  const existingUsers = Object.keys(
    previousSnapshot.participants,
  ).filter(user =>
    new BN(previousSnapshot.participants[user].balance, 'hex').gt(zero),
  );

  let history: { [user: string]: UserHistory[] } = {};

  await Promise.all(
    existingUsers.map(async user => {
      const allRedirectionEvents = await getAllRedirectInfo(
        web3,
        Atoken,
        undefined,
        { _from: user },
        previousSnapshot.endBlock + 1, // start block
        toBlock, // end block
        decimals,
      );
      const userRedirectionHistory = createUserRedirectionHistory(
        allRedirectionEvents,
        true,
        endTimestamp,
        startTimestamp,
        new BN(previousSnapshot.participants[user].balance, 'hex'),
      );
      history[user] = userRedirectionHistory;
    }),
  );

  const usersRedirectionEvents = await getAllRedirectInfo(
    web3,
    Atoken,
    undefined,
    { _to: env.redirectionAddress },
    previousSnapshot.endBlock + 1, // start block
    toBlock, // end block
    decimals,
  );

  const newUsers: string[] = [];

  for (let event of usersRedirectionEvents.array) {
    if (
      existingUsers.find(function (element) {
        return element === event.from;
      }) === undefined
    ) {
      newUsers.push(event.from);
    }
  }

  await Promise.all(
    newUsers.map(async user => {
      const allRedirectionEvents = await getAllRedirectInfo(
        web3,
        Atoken,
        undefined,
        { _from: user },
        previousSnapshot.endBlock + 1, // start block
        toBlock, // end block
        decimals,
      );
      const userRedirectionHistory = createUserRedirectionHistory(
        allRedirectionEvents,
        false,
        endTimestamp,
        startTimestamp,
        undefined,
      );
      history[user] = userRedirectionHistory;
    }),
  );

  const newSnapshot: SnapshotWithName = {
    name: tokenName,
    startBlock: previousSnapshot.endBlock + 1,
    endBlock: toBlock,
    startingTime: startTimestamp,
    endingTime: endTimestamp,
    participants: Object.fromEntries(
      existingUsers
        .concat(newUsers)
        .map(address => [address, { balance: zero, weight: zero }]),
    ),
  };

  await Promise.all(
    Object.keys(history).map(async user => {
      let userOperation: UserOperation[] = [];
      const allDepositEvents = await getAllDepositInfo(
        web3,
        Atoken,
        undefined,
        { _from: user },
        previousSnapshot.endBlock + 1, // start block
        toBlock, // end block
        decimals,
      );
      const deposits: UserOperation[] = allDepositEvents.array.map(event => {
        return {
          type: 'Deposit',
          timestamp: event.blockTimestamp,
          amount: new BN(event.amount),
        };
      });
      const allWithdrawEvents = await getAllWithdrawInfo(
        web3,
        Atoken,
        undefined,
        { _from: user },
        previousSnapshot.endBlock + 1, // start block
        toBlock, // end block
        decimals,
      );
      const withdraws: UserOperation[] = allWithdrawEvents.array.map(event => {
        return {
          type: 'Withdraw',
          timestamp: event.blockTimestamp,
          amount: new BN(event.amount),
        };
      });
      const allInTransferEvents = await getAllTransferInfo(
        web3,
        Atoken,
        undefined,
        { _to: user },
        previousSnapshot.endBlock + 1, // start block
        toBlock, // end block
        decimals,
      );
      const inTransfer: UserOperation[] = allInTransferEvents.array.map(
        event => {
          return {
            type: 'InTransfer',
            timestamp: event.blockTimestamp,
            amount: new BN(event.amount),
          };
        },
      );
      const allOutTransferEvents = await getAllTransferInfo(
        web3,
        Atoken,
        undefined,
        { _from: user },
        previousSnapshot.endBlock + 1, // start block
        toBlock, // end block
        decimals,
      );
      const outTransfer: UserOperation[] = allOutTransferEvents.array.map(
        event => {
          return {
            type: 'OutTransfer',
            timestamp: event.blockTimestamp,
            amount: new BN(event.amount),
          };
        },
      );
      const operation = deposits
        .concat(withdraws)
        .concat(inTransfer)
        .concat(outTransfer)
        .sort(function (operationA, operationB) {
          return operationA.timestamp - operationB.timestamp;
        });
      operationsRefactor(operation, history[user]);
      const userWeights = history[user].map(interval =>
        interval.amount.mul(new BN(interval.endTime - interval.startTime)),
      );
      const userTotalWeight = userWeights.reduce((weightA, weightB) =>
        weightA.add(weightB),
      );
      newSnapshot.participants[user].weight = userTotalWeight;
      const lastUserBalance = lastBalance(history[user], endTimestamp);
      newSnapshot.participants[user].balance =
        lastUserBalance === null ? zero : lastUserBalance;
    }),
  );
  return newSnapshot;
}

function createUserRedirectionHistory(
  redirections: SortedArray<TimestampedRedirectEvent, number>,
  userExists: boolean,
  endingTime: number,
  startingTime?: number,
  previousAmount?: BN,
): UserHistory[] {
  let redirectUserHistory: UserHistory[] = [];
  if (userExists) {
    redirectUserHistory.push({
      startTime: startingTime,
      endTime: 0,
      amount: new BN(previousAmount),
    });
  }
  let isRedirected = userExists;

  for (let event of redirections.array) {
    if (isRedirected && event.to !== env.redirectionAddress) {
      redirectUserHistory[redirectUserHistory.length - 1].endTime =
        event.blockTimestamp;
      isRedirected = false;
    }
    if (!isRedirected && event.to === env.redirectionAddress) {
      redirectUserHistory.push({
        startTime: event.blockTimestamp,
        endTime: 0,
        amount: new BN(event.amount),
      });
      isRedirected = true;
    }
  }
  if (redirectUserHistory[redirectUserHistory.length - 1].endTime === 0) {
    redirectUserHistory[redirectUserHistory.length - 1].endTime = endingTime;
  }
  return redirectUserHistory;
}

function operationsRefactor(
  operations: UserOperation[],
  history: UserHistory[],
) {
  for (let operation of operations) {
    const timeIntervalIndex = findInterval(operation.timestamp, history);
    if (timeIntervalIndex !== null) {
      const newAmount =
        operation.type === 'Deposit' || operation.type === 'InTransfer'
          ? history[timeIntervalIndex].amount.add(operation.amount)
          : history[timeIntervalIndex].amount.sub(
              history[timeIntervalIndex].amount.gt(operation.amount)
                ? operation.amount
                : history[timeIntervalIndex].amount,
            );
      history.push({
        startTime: operation.timestamp,
        endTime: history[timeIntervalIndex].endTime,
        amount: newAmount,
      });
      history[timeIntervalIndex].endTime = operation.timestamp;
    }
  }
}

function findInterval(
  operationTimestamp: number,
  history: UserHistory[],
): number | null {
  for (let j = 0; j < history.length; j++) {
    if (
      history[j].startTime <= operationTimestamp &&
      history[j].endTime >= operationTimestamp
    ) {
      return j;
    }
  }
  return null;
}

function lastBalance(history: UserHistory[], endingTme: number): BN | null {
  for (let j = 0; j < history.length; j++) {
    if (history[j].endTime === endingTme) {
      return history[j].amount;
    }
  }
  return null;
}

export type TotalDeposited = {
  total: BN;
  pools: { [name: string]: BN };
};

export function calculateTotalDeposited(
  snapshots: SnapshotWithName[],
): TotalDeposited {
  return snapshots.reduce<TotalDeposited>(
    (result, { name, participants }) => {
      const totalPerPool = mapSumBN(
        Object.values(participants),
        x => x.balance,
      );
      result.pools[name] = totalPerPool;
      result.total = result.total.add(totalPerPool);
      return result;
    },
    { total: zero, pools: {} },
  );
}

export function calculateAggregate(snapshots: SnapshotWeightsOnly[]) {
  let aggregateWeights: { [address: string]: { weight: BN } } = {};
  for (let snapshot of snapshots) {
    for (let user of Object.keys(snapshot.participants)) {
      aggregateWeights[user] = {
        weight: (aggregateWeights[user]?.weight ?? zero).add(
          snapshot.participants[user].weight,
        ),
      };
    }
  }
  const aggregateSnapshot = {
    startBlock: snapshots[0].startBlock, // the first block included in the interval (inclusive) | 05:00:10
    endBlock: snapshots[0].endBlock,
    startingTime: snapshots[0].startingTime, // the last block included in the interval (inclusive)  | 05:59:57
    endingTime: snapshots[0].endingTime,
    participants: aggregateWeights,
  };
  return aggregateSnapshot;
}
