import BN from 'bn.js';
import { isAddress, toWei, fromWei } from 'web3-utils';
import { promises as fs } from 'fs';
import path from 'path';
import _ from 'lodash';
import {
  Recipient,
  groupByAddressAndSortByAmount,
  recipientListFromJson,
  saveRecipientsFile,
} from './models';
import { SortedArray } from './sorting';

export function assertIsUndefinedOrFiniteFloat(num: unknown): number | null {
  if (num === null || num === void 0) return undefined;
  else return parseFiniteFloat(num);
}

export function assertIsUndefinedOrInteger(num: unknown): number | null {
  if (num === null || num === void 0) return undefined;
  else return parseInteger(num);
}

export function assertIsFiniteNumber(num: number): number {
  if (!Number.isFinite(num))
    throw new Error(`value='${num}' is not a finite number.`);
  return num;
}

export function parseFiniteFloat(num: unknown): number {
  const result =
    typeof num === 'number' ? num : Number.parseFloat(assertIsString(num));
  return assertIsFiniteNumber(result);
}

export function parseInteger(num: unknown): number {
  const result =
    typeof num === 'number' ? num : Number.parseFloat(assertIsString(num));
  if (!Number.isInteger(result))
    throw new Error(`value='${num}' is not an integer.`);
  return result;
}

export function assertIsBoolean(value: unknown): boolean {
  if (value === true || value === false) return value;
  else throw new Error(`value='${value}' is not boolean.`);
}

export function throwError(message: string): never {
  throw new Error(message);
}

export function isAddressZero(address: string) {
  return /^(0x)?0{40}$/.test(address);
}

export function assertIsAddress(val: unknown): string {
  const str = assertIsString(val);
  if (!isAddress(str))
    throw new Error(`value='${str}' is not a valid Ethereum address.`);
  return str;
}

export function assertIsArray(val: unknown): unknown[] {
  if (!_.isArray(val)) {
    throw new Error(`'${JSON.stringify(val)}' is not an array.`);
  }
  return val;
}

type Object = { [prop: string]: unknown } & { [prop: number]: unknown };

export function assertIsObject(val: unknown): Object {
  if (!_.isObject(val)) {
    throw new Error(`'${JSON.stringify(val)}' is not an array.`);
  }
  return val as Object;
}

export function assertIsNumber(val: unknown): number {
  if (typeof val !== 'number') {
    throw new Error(`value=${val} is not a number.`);
  }
  return val;
}

export function assertIsString(val: unknown, minLength = 1): string {
  if (typeof val !== 'string' || val.length < minLength) {
    throw new Error(
      `value=${JSON.stringify(
        val,
      )} is not a string of at least ${minLength} characters.`,
    );
  }
  return val;
}

export function mapReduce<T, U, R>(
  array: T[],
  initialValue: R,
  mapFn: (elem: T) => U,
  reduceFn: (curr: R, next: U) => R,
) {
  return array.reduce(
    (curr, next) => reduceFn(curr, mapFn(next)),
    initialValue,
  );
}

export const zero = new BN(0);

export function mapSumBN<T>(array: T[], getter: (elem: T) => BN) {
  return mapReduce(array, zero, getter, (curr, next) => curr.add(next));
}

export function sumBN(listOfNumbers: BN[]) {
  return mapSumBN(listOfNumbers, x => x);
}

export function weiToNumber(wei: string) {
  return fromBNToNumber(new BN(wei));
}

export function fromBNToDecimalString(bn: BN) {
  return fromWei(bn);
}

export function formatBN(bn: BN) {
  return fromBNToNumber(bn).toFixed(2);
}

export function fromBNToNumber(bn: BN) {
  return Number.parseFloat(fromBNToDecimalString(bn));
}

export function toBN(str: string) {
  return new BN(toWei(str.replace(/,/g, '')));
}

export function replaceBN(obj: {}) {
  if (BN.isBN(obj)) {
    return formatBN(obj);
  }
  else if (typeof obj !== 'object') {
    return obj;
  }
  const result: any = {};
  for (const [key, value] of Object.entries(obj)) {
    result[key] = replaceBN(value);
  }
  return result;
}

export async function writeJsonToFile(filePath: string, json: any) {
  return await fs.writeFile(filePath, JSON.stringify(json, null, 2) + '\n');
}

export async function readJsonFromFile(filePath: string): Promise<unknown> {
  return JSON.parse(await fs.readFile(filePath, 'utf-8'));
}

export function loadJSON(filePath: string) {
  return require(path.resolve(filePath));
}

export async function normailzeRecipientList(
  list: unknown[],
): Promise<SortedArray<Recipient, string>> {
  const recipients = recipientListFromJson(list);
  console.log(`Grouping recipients by address...`);

  const res = groupByAddressAndSortByAmount(recipients);

  await saveRecipientsFile(res.array, './investors_sorted.json');

  console.log(
    `  -> Before grouping: ${recipients.length}, after: ${res.length}.`,
  );
  return res;
}

interface BountyInfo {
  addresses: string[];
  tokens: string[];
}

export function parseBountyList(filePath: string) {
  const bounty = require(filePath) as BountyInfo; // TODO: verify
  const recipients = bounty.addresses;
  const tokens = bounty.tokens.map(t => new BN(t, 10));
  return { recipients, tokens };
}

export async function mergeJSONFiles(dir: string) {
  return _.chain(await fs.readdir(path.resolve(dir)))
    .map(file => require(file))
    .flatten();
}
