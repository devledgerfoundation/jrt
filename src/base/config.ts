require('dotenv').config();

const {
  INFURA_PROJECT_ID,
  DEV_MNEMONIC,
  ETHERSCAN_API_KEY,
  LP_SLOT_DURATION,
  SP_SLOT_DURATION,
  JARVIS_REDIRECTION_ADDRESS,
} = process.env;

export const env = {
  constants: {
    BATCH_TRANSACTION_COUNT: 225,
  },
  secrets: {
    DEV_MNEMONIC,
  },
  apiKeys: {
    infura: INFURA_PROJECT_ID,
    etherscan: ETHERSCAN_API_KEY,
  },
  ethAddresses: {
    deployingWallet: '0x0D54AAdd7CE2DC10eb9527C6105a3c3f1b463d1b',
    jrtContract: '0x8A9C67fee641579dEbA04928c4BC45F66e26343A',
    jrtBancorPool: '0x4827e558e642861Cd7a1C8f011b2B4661F8d51fa',
    liquidityPools: {
      bancor_usdb: '0x4827e558e642861Cd7a1C8f011b2B4661F8d51fa',
      bancor_bnt: '0x069D653038DB2F9d84e9620Be140B3D404a40258',
      bancor_bnt10_jrt90: '0xE274C0cCf7B0bC1eF29FFf9AD5eC98E9B5c45e84',
      uniswap_v1_JRT_ETH: '0x634e27269a029edb2b70b5ef43273f5eed9ebcc2',
      uniswap_v2_JRT_ZRX: '0xdaaa2aa9a2b9c8d0aca8eff10370e215f41d297a',
      uniswap_v2_JRT_USDC: '0xcd0033b0a30bc0686dcba72d62d4e0d44790a960',
      uniswap_v2_JRT_ETH: '0x2b6a25f7c54f43c71c743e627f5663232586c39f',
      uniswap_v2_DAI_JRT: '0xe4a356afd925f73f3dd22acbeb2c0c87a05e895d',
      uniswap_v2_JRT_BAL: '0x43C3f51B1Ac228b0F5f39f0EffEF0cb1895A82f0',
      balancer_dai_jrt_001: '0x49e4CB1BEBc5A060505d4571Ec50bd2B65c3ed11',
      balancer_weth_jrt_001: '0x68198e4ed6975204B3467dc217166d9ff1CbB57A',
      balancer_usdc_jrt_001: '0x01d7E6A79F6E6Dc6f0884743078F76AC1239520A',
      balancer_jrt70_usdc30_001: '0xB537aE8eebe8a7bEC9eE8720C936Fd15C35940fE',
      balancer_jrt70_dai30_001: '0xdB56E9035972117d532b5AB0E261ba0aBf8Dab4B',
      balancer_jrt70_weth30_001: '0xb454f3f2b982e56b962392d93d01e5895b532995',
      balancer_jrt70_snx30_001: '0xd21Ae4B071055a3294A9f41b34Fa0B7f1FfBA647',
      balancer_ausdt10_jrt90_001: '0x48b19e619a99058c4c1dde931632bb87f4fb98ad',
      balancer_susd10_jrt90_001: '0x3b53cd95f2c1f228c96e8426ad81bae79e4bb714',
      balancer_dai49_jrt51_001: '0x1cbf3f656929202442a44774643356e3ef1c799c',
      balancer_jrt90_snx10_001: '0x0e4383eb32d32f8a23476cb4910a6af89c91ef70',
      balancer_jrt90_eth10: '0xe3971f632d733cd3a2293c91aec501d6fb685939',
      balancer_jrt70_comp30_001: '0xf984bb6ca32f452bb21ddbfa99ec62dc4ce35c2d',
      balancer_bnt25_jrt75_001: '0x954645b1534f22ef6ed69d291e1988470785361f',
      balancer_jrt25_dmg25_delta30_usdc10_trb10_001:
        '0xd386bb106E6FB44F91E180228EDECA24EF73C812',
      balancer_jrt60_uma32_dxd8_001:
        '0x5508C27321AbA85F1af73a2153F4A16c8616FEa4',
      balancer_JRT27_aUSDC35_cUSDT35_COMP3_001:
        '0x8EA198B47A3abD30A10c0df4cCD7036Cf96CC4E2',
    },
  },
  lpSlotDuration: LP_SLOT_DURATION,
  AaveTokens: {
    DAI: {
      address: '0xfC1E690f61EFd961294b3e1Ce3313fBD8aa4f85d',
      decimals: 18,
    },
    USDT: {
      address: '0x71fc860F7D3A592A4a98740e39dB31d25db65ae8',
      decimals: 6,
    },
    USDC: {
      address: '0x9bA00D6856a4eDF4665BcA2C2309936572473B7E',
      decimals: 6,
    },
    SUSD: {
      address: '0x625aE63000f46200499120B906716420bd059240',
      decimals: 18,
    },
    TUSD: {
      address: '0x4DA9b813057D04BAef4e5800E36083717b4a0341',
      decimals: 18,
    },
    BUSD: {
      address: '0x6Ee0f7BB50a54AB5253dA0667B0Dc2ee526C30a8',
      decimals: 18,
    },
  },
  spSlotDuration: SP_SLOT_DURATION,
  redirectionAddress: JARVIS_REDIRECTION_ADDRESS,
};
