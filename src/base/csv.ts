import BN from 'bn.js';
import fs from 'fs';
import parse from 'csv-parse/lib/sync';
import { assertIsString, assertIsArray, assertIsAddress } from './utils';
import { hasProp } from './meta';
import { RecipientJsonEntry, Holder } from './models';

export function parseCsv(path: string): unknown[] {
  console.log(`Reading CSV file: '${path}'`);
  const file = fs.readFileSync(path, 'utf8');
  const records = parse(file, {
    columns: true,
    skip_empty_lines: true,
  });
  return assertIsArray(records);
}

export function parseCsvRecipientList(path: string): RecipientJsonEntry[] {
  const records = parseCsv(path).map(
    ({ address, amount }: RecipientJsonEntry) => {
      address = assertIsString(address);
      amount =
        amount.search(/[eE]/) != -1
          ? Number.parseFloat(amount).toFixed(18)
          : amount;
      return { address, amount };
    },
  );

  return records as RecipientJsonEntry[];
}

export function parseCsvErc20Snapshot(path: string): Holder[] {
  return parseCsv(path).map(x => {
    if (
      !(typeof x === 'object' && hasProp(x, 'address') && hasProp(x, 'balance'))
    ) {
      throw new Error();
    }
    return {
      address: assertIsAddress(x.address),
      balance: new BN(assertIsString(x.balance)),
    };
  });
}

export function holdersAsCsv(holders: Holder[]): string {
  let result = 'address,balance\n';
  for (let holder of holders) {
    result += `${holder.address.toLowerCase()},${holder.balance.toString(
      10,
    )}\n`;
  }
  return result;
}
