import Web3 from 'web3';
import {
  getContractAbiFromArtifacts,
  Web3Source,
  getContract,
} from '../base/web3';
import { AbstractErc20 } from '../base/erc20';
import { env } from '../base/config';
import { getWeb3ForDeployer } from './deployment';

export function getJrtAbi() {
  return getContractAbiFromArtifacts('JarvisRewardToken');
}

export async function getJrt(
  web3OrNetwork: Web3Source,
  gasOptions?: { gasLimit: number; gasPrice: string },
): Promise<AbstractErc20> {
  return await getContract<AbstractErc20>(
    env.ethAddresses.jrtContract,
    web3OrNetwork,
    { type: 'direct', abi: getJrtAbi() },
    gasOptions,
  );
}

export async function getJrtAndWeb3({
  gasLimit,
  optionalGasPrice,
  customRpcUrl,
}: {
  gasLimit?: number;
  optionalGasPrice?: number;
  customRpcUrl?: string;
} = {}) {
  const { web3, stopWeb3, gasPrice } = await getWeb3ForDeployer({
    networkId: 1,
    ledger: true,
    gasPrice: optionalGasPrice,
    customRpcUrl,
  });
  const jrt = await getJrt(web3, { gasLimit, gasPrice: `${gasPrice}` });
  return { jrt, web3, stopWeb3 };
}
