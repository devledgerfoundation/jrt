import { LedgerProvider } from '@jarvis-network/ledger-web3-provider';
const HDWalletProvider = require('@truffle/hdwallet-provider');
import Web3 from 'web3';
import { getInfuraEndpoint } from '../apis/infura';
import { env } from '../base/config';
import { NetworkId } from '../base/networks';
import { getBestEthGasPrice } from '../apis/ethgasstation';

type Web3ProviderParams = {
  networkId: NetworkId;
  customRpcUrl?: string;
  ledger: boolean;
  gasLimit?: number;
  gasPrice?: number;
};

export function makeNetworkDef({
  networkId,
  customRpcUrl,
  ledger,
  gasPrice,
  gasLimit = 1_500_000,
}: Web3ProviderParams) {
  const rpcUrl = customRpcUrl ?? getInfuraEndpoint(networkId);

  console.log(`Connecting to RPC endpoint: '${rpcUrl}' | using ledger: ${ledger}`);

  if (!gasPrice) {
    // Any value of `gasPrice` will do, as this branch is only taken when
    // truffle/open-zeppelin-cli imports `truffle-config.js` when compiling the
    // Solidity contracts.
    gasPrice = networkId == 1 ? 50e9 : 1e9;
  }

  let provider = !ledger
    ? new HDWalletProvider(env.secrets.DEV_MNEMONIC, rpcUrl)
    : new LedgerProvider({
        networkId,
        paths: ["44'/60'/x'/0/0"],
        askConfirm: false,
        accountsLength: 1,
        accountsOffset: 0,
        rpcUrl,
      });

  return {
    gasPrice,
    gas: gasLimit,
    provider,
    network_id: networkId,
    stopWeb3: () => {
      if (ledger) {
        (provider as LedgerProvider).stop();
      }
    },
  };
}

export async function getWeb3ForDeployer({
  customRpcUrl,
  networkId,
  ledger,
  gasPrice,
}: Web3ProviderParams) {
  gasPrice = gasPrice || (await getBestEthGasPrice()).bestPriceWei;
  const { provider, stopWeb3 } = makeNetworkDef({
    customRpcUrl,
    networkId,
    ledger,
  });
  return {
    web3: new Web3(provider),
    stopWeb3,
    gasPrice,
  };
}
