require('./runtime_config');
import program from 'commander';
import { assertIsString, assertIsAddress } from '../base/utils';
import { parseCsv } from '../base/csv';
import { fromWei, toWei } from 'web3-utils';
import { promises as fs } from 'fs';

program
  .requiredOption(
    '-i, --input <path>',
    'Path to a JSON file containing array of recipients',
  )
  .requiredOption('-o, --output <path>', 'Output path')
  .parse(process.argv);

async function main() {
  const inputPath = assertIsString(program.input);
  const outputPath = assertIsString(program.output);

  console.log('-------------');
  console.log(`Reading '${inputPath}' and saving to: '${outputPath}'.`);

  const list = parseCsvRecipeintList(inputPath);
  const result = convertToCsv(list);

  await fs.writeFile(outputPath, result);
  console.log('Program execution complete.\n');
}

(async () => await main())();

function parseCsvRecipeintList(path: string) {
  const records = parseCsv(path).map(
    ({ address, balance }: { address: string; balance: string }) => {
      address = assertIsAddress(address.trim());
      balance = balance.trim().replace(',', '');
      console.log(`Input: ${balance}`);
      balance =
        // 1E18 -> 0.0000000000001
        balance.search(/[eE]/) != -1
          ? Number.parseFloat(balance).toFixed(18)
          : balance;
      balance = toWei(balance);
      return { address, balance };
    },
  );

  return records;
}

function convertToCsv(holders: { address: string; balance: string }[]) {
  let result = 'address,balance\n';
  for (let holder of holders) {
    result += `${holder.address.toLowerCase()},${holder.balance}\n`;
  }
  return result;
}
