import { promises as fs } from 'fs';
import { getBestEthGasPrice } from '../apis/ethgasstation';

(async () => {
  const prices = await getBestEthGasPrice();
  await fs.writeFile('./data/gas_prices.json', JSON.stringify(prices, null, 2));
})();
