require('./runtime_config');
import _ from 'lodash';
import { promises as fs } from 'fs';
import program from 'commander';
import {
  sumAmounts,
  saveRecipientsFile,
  readRecipientsFile,
  groupByAddressAndSortByAmount,
} from '../base/models';
import { assertIsString, formatBN } from '../base/utils';

program
  .requiredOption(
    '-i, --input <dir>',
    'Path to a folder containing JSON recipient files to group',
  )
  .requiredOption(
    '-o, --output <path>',
    'Output filepath for the aggregate list',
  )
  .parse(process.argv);

async function main() {
  console.log('-------------');

  const inputPath = assertIsString(program.input);
  const outputPath = assertIsString(program.output);

  const files = await fs.readdir(inputPath);

  console.log(
    `Reading ${files.length} entries from '${inputPath}', and saving the result to: '${outputPath}'.`,
  );

  const listOfLists = await Promise.all(
    files.map(async filename => await readRecipientsFile(`${inputPath}/${filename}`)),
  );

  const flatList = _.flatten(listOfLists);

  const result = groupByAddressAndSortByAmount(flatList).array;

  const afterTotal = formatBN(sumAmounts(result));
  console.log(
    `  -> After grouping: '${result.length}' entries totaling: '${afterTotal}'.`,
  );

  await saveRecipientsFile(result, outputPath);
  console.log('Program execution complete.\n');
}

(async () => await main())();
