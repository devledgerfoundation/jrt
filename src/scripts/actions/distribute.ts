import BN from 'bn.js';
import Web3 from 'web3';
import { env } from '../../base/config';
import { normailzeRecipientList, formatBN } from '../../base/utils';
import { t } from '../../base/meta';
import { Distributor } from '../../distributor';
import { Recipient, sumAmounts } from '../../base/models';
import { AbstractErc20 } from '../../base/erc20';
import { estimateFee } from '../../base/web3';
import { getJrtAndWeb3 } from '../../jrt-specific/get-jrt';
import { assert } from 'console';
import { toWei } from 'web3-utils';

type DistributionListParams = {
  recipients: Recipient[];
  startIndex?: number;
  endIndex?: number;
  batchSize?: number;
};

type DistributeJrtTokensParams = {
  listParams: DistributionListParams;
  verify?: boolean;
  dryRun?: boolean;
  pause?: boolean;
  gasPrice?: number;
  gasLimit?: number;
  customRpcUrl?: string;
};

export async function distributeJrtTokens({
  gasPrice,
  gasLimit,
  customRpcUrl,
  listParams: initialListParams,
  ...params
}: DistributeJrtTokensParams) {
  const [estimatedGasLimit, listParams] = calculateGasLimit(initialListParams);
  const { jrt, web3, stopWeb3 } = await getJrtAndWeb3({
    gasLimit: gasLimit ?? estimatedGasLimit,
    optionalGasPrice: gasPrice,
    customRpcUrl,
  });
  await distributeTokens({ erc20: jrt, web3, listParams, ...params });
  stopWeb3();
}

function calculateGasLimit({
  recipients,
  startIndex = 0,
  endIndex = recipients.length,
  batchSize = env.constants.BATCH_TRANSACTION_COUNT,
}: DistributionListParams): [number, DistributionListParams] {
  assert(
    startIndex >= 0 && startIndex <= endIndex && endIndex <= recipients.length,
  );
  const initialTxCost = 21_000;
  const proxyContractOverhead = 10_000;
  const erc20TransferCost = 30_000;
  const recipientsCount = Math.min(endIndex - startIndex, batchSize);
  const gasLimit =
    initialTxCost + proxyContractOverhead + recipientsCount * erc20TransferCost;
  return t(gasLimit, { startIndex, endIndex, batchSize, recipients });
}

type DistributeTokensParams = DistributeJrtTokensParams & {
  web3: Web3;
  erc20: AbstractErc20;
};

export async function distributeTokens({
  erc20,
  web3,
  verify = true,
  dryRun = false,
  pause = false,
  listParams,
}: DistributeTokensParams) {
  const { recipients } = listParams;
  if (!recipients.length) {
    console.log(`Recipient list empty, skipping distribution process.`);
    return;
  }
  const sym = await erc20.methods.symbol().call();
  const walletAddress = env.ethAddresses.deployingWallet;
  console.log('================');
  console.log(`Using ${sym} deployed at '${erc20.options.address}'.`);
  console.log(`Sending transactions from: '${walletAddress}'.`);
  const sortedRecipients = await normailzeRecipientList(recipients);
  const count = sortedRecipients.length;
  const totalTokens = sumAmounts(sortedRecipients.array);
  console.log(
    `  -> ${count} recipients, equating to: ${formatBN(totalTokens)} tokens.`,
  );
  const tokenBalance = new BN(
    await erc20.methods.balanceOf(walletAddress).call(),
  );
  if (tokenBalance.lt(totalTokens)) {
    throw new Error(
      `Insufficient wallet token balance: needs ${formatBN(
        totalTokens,
      )} ${sym}, ` + `have ${formatBN(tokenBalance)} ${sym}`,
    );
  }
  const estimations = await printTxFeeInfoAndGetEstimator(erc20);
  const fees = new BN(toWei(estimations.maxFeeEth));
  const balance = new BN(await web3.eth.getBalance(walletAddress));
  if (balance.lt(fees)) {
    throw new Error(
      `Insufficient wallet balance: needs ${formatBN(fees)} ETH, ` +
        `have ${formatBN(balance)} ETH`,
    );
  }
  const d = new Distributor(
    web3,
    sortedRecipients,
    walletAddress,
    erc20,
    estimations.estimateFeesForTx,
  );

  if (!dryRun) {
    await d.unpause();
    await d.distribute(
      listParams.startIndex,
      listParams.endIndex,
      listParams.batchSize,
    );
    if (pause) {
      await d.pause();
    }
  }
  if (verify) {
    await d.verifyBalances(0, count);
  }
}

async function printTxFeeInfoAndGetEstimator(erc20: AbstractErc20) {
  const estimations = await estimateFee(erc20);
  const {
    gasPriceGwei,
    gasLimit,
    usdEthRate,
    maxFeeUsd,
    maxFeeEth,
    usdEthPriceTimestamp,
  } = estimations;
  console.log(
    `Gas limit = ${gasLimit} | gasPrice = ${gasPriceGwei} | Max fee = $${maxFeeUsd} USD (${maxFeeEth} ETH))`,
  );
  console.log(
    `  (USD/ETH Price: ${usdEthRate}, as of ${usdEthPriceTimestamp})`,
  );
  return estimations;
}
