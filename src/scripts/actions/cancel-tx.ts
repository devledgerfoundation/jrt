import Web3 from 'web3';
import { once, logTransactionStatus } from '../../base/web3';

export async function cancelTx(web3: Web3, nonce: number, gasPrice: number) {
  const [myAddress] = await web3.eth.getAccounts();
  console.log(
    `Sending tx: from = '${myAddress}' | gas price = '${
      gasPrice / 1e9
    } Gwei' | nonce = '${nonce}'`,
  );

  const maxConfirmations = 2;
  web3.eth.transactionConfirmationBlocks = maxConfirmations;

  const promiEvent = web3.eth.sendTransaction({
    from: myAddress,
    to: myAddress,
    value: 0,
    nonce,
    gasPrice,
  });

  await logTransactionStatus(web3, promiEvent);
}
