import _ from 'lodash';
import BN from 'bn.js';
import { readdirSync } from 'fs';
import { toBN, loadJSON, zero } from '../../base/utils';
import { formatDate, parseDate } from '../../base/formatting';
import {
  findElementWithClosestTimestamp,
  addSeconds,
} from '../../base/date-time-utils';
import type { ParticipantWeights } from '../../calculations/snapshot';

export function calculateTotalReward(
  beforeDate: Date,
  durationInSeconds: number,
  pool: string,
) {
  const rewardSizeDir = 'data/reward-size';
  const dates = readdirSync(rewardSizeDir);
  const afterDate = addSeconds(beforeDate, durationInSeconds);
  const closestDate = findElementWithClosestTimestamp(
    dates,
    afterDate,
    parseDate,
  );

  if (closestDate[0] < 0 || closestDate[0] >= dates.length)
    throw new Error(
      `No matching reward-size file for date '${formatDate(afterDate)}'`,
    );

  const filename = `${rewardSizeDir}/${closestDate[1]}`;
  console.log(`Using rewards file from '${filename}'`);
  const rewardInfo = loadJSON(filename);
  // console.log(rewardInfo);
  if (rewardInfo[pool] == null || rewardInfo[pool] == 0) {
    console.log(
      `Skipping pool '${pool}' as its reward size was not found or zero`,
    );
    return null;
  } else {
    return toBN(rewardInfo[pool]);
  }
}

export function calculateUserReward(participants: ParticipantWeights, reward: BN) {
  const userWeight = Object.keys(participants).map(user => {
    return { address: user, weight: participants[user].weight };
  });
  const totalWeights = userWeight
    .map(userPosition => userPosition.weight)
    .reduce((a, b) => a.add(b));
  const tempRecipients = userWeight.map(userPosition => {
    return {
      address: userPosition.address,
      amount: reward.mul(userPosition.weight).div(totalWeights),
    };
  });
  const recipients = tempRecipients.filter(recipient =>
    recipient.amount.gt(zero),
  );
  return recipients;
}
