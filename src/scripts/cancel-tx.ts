require('./runtime_config');
import program from 'commander';
import { getWeb3ForDeployer } from '../jrt-specific/deployment';
import { cancelTx } from './actions/cancel-tx';

program
  .requiredOption(
    '--gas-price <number>',
    'Gas price (in GWei) for the cancel tx',
  )
  .requiredOption(
    '--nonce <number>',
    'Nonce of the cancel tx',
  )
  .parse(process.argv);

async function main() {
  const gasPrice = Number.parseFloat(program.gasPrice) * 1e9;
  const nonce = Number.parseFloat(program.nonce);
  const { web3, stopWeb3 } = await getWeb3ForDeployer({
    networkId: 1,
    ledger: true,
  });
  try {
    await cancelTx(web3, nonce, gasPrice);
  } catch(err) {
    console.log(`Sending tx failed. err: `, err);
  }
  console.log(`Program execution complete.`);
  stopWeb3();
}

(async () => await main())();
