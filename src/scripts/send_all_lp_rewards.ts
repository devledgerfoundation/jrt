require('./runtime_config');
import program from 'commander';
import { assertIsString } from '../base/utils';
import { calculateAllRewards } from './actions/liquidity-reward-calculation';
import { makeErc20Snapshot } from './actions/make-erc20-snapshot';
import { parseDateTime } from '../base/formatting';
import { env } from '../base/config';
import { distributeJrtTokens } from './actions/distribute';

program
  .requiredOption('--date <date>', 'Date relative to which to calculate tokens')
  .option(
    '--exec',
    'Execute distribution immediately after calculations',
    false,
  )
  .option('--verify', 'Perform verification', false)
  .option(
    '--make-snapshot',
    'Whether to perform snapshot before calculating the distribution list',
    false,
  );

program.parse(process.argv);

(async () => {
  console.log('Starting program.');
  if (program.makeSnapshot === true) {
    await Promise.all(
      Object.entries(env.ethAddresses.liquidityPools).map(([pool, address]) =>
        makeErc20Snapshot(pool, address),
      ),
    );
  }
  const pools = await calculateAllRewards(
    parseDateTime(assertIsString(program.date)),
  );
  if (!program.exec && !program.verify) return;

  console.log(
    `Starting token ${program.exec ? 'distribution' : 'verification'}.`,
  );
  for (let pool of pools) {
    if (!pool.distributionPlan) throw new Error('Distribution plan missing!');
    console.log(`Sending tokens for pool '${pool.name}'`);
    await distributeJrtTokens({
      listParams: {
        recipients: pool.distributionPlan.recipients,
      },
      dryRun: !program.exec,
      verify: program.verify,
    });
  }
  console.log('Program execution complete.');
})();
