require('./runtime_config');
import BN from 'bn.js';
import program from 'commander';
import {
  adjustAmounts,
  sumAmounts,
  saveRecipientsFile,
  readRecipientsFile,
} from '../base/models';
import { assertIsString, formatBN } from '../base/utils';

program
  .requiredOption(
    '-i, --input <path>',
    'Path to a JSON file containing array of recipients',
  )
  .requiredOption(
    '--scale <number>',
    "Scalar to multiply each recipient's amount with",
  )
  .option('-o, --output <path>', 'Output path')
  .parse(process.argv);

async function main() {
  const scale = Number.parseFloat(program.scale);
  const inputPath = assertIsString(program.input);
  const outputPath = assertIsString(program.output);
  console.log('-------------');
  console.log(
    `Reading '${inputPath}', multiplying by '${scale}' and saving to: '${outputPath}'.`,
  );

  const list = await readRecipientsFile(inputPath);

  const beforeTotal = formatBN(sumAmounts(list));
  console.log(`  -> Read '${list.length}' entries totaling: '${beforeTotal}'.`);

  const result = adjustAmounts(list, amount => amount.mul(new BN(scale)));

  const afterTotal = formatBN(sumAmounts(result));
  console.log(
    `  -> After scaling: '${result.length}' entries totaling: '${afterTotal}'.`,
  );

  await saveRecipientsFile(result, outputPath);
  console.log('Program execution complete.\n');
}

(async () => await main())();
