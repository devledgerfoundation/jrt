require('./runtime_config');
import { promises as fs } from 'fs';
import program from 'commander';
import {
  assertIsFiniteNumber,
  assertIsString,
  readJsonFromFile,
  replaceBN,
  writeJsonToFile,
} from '../base/utils';
import {
  calculateTotalReward,
  calculateUserReward,
} from './actions/weight-reward-calculation';
import {
  AaveCalculate,
  calculateAggregate,
  calculateTotalDeposited,
} from '../calculations/sponsoring-pool';
import type { Snapshot, SnapshotWithName } from '../calculations/snapshot';
import { parseDateTime, formatDate, parseDate } from '../base/formatting';
import { addSeconds } from '../base/date-time-utils';
import { env } from '../base/config';
import type { AToken } from '../contracts/AToken';
import { getContract, getWeb3 } from '../base/web3';
import {
  groupByAddressAndSortByAmount,
  saveRecipientsFile,
} from '../base/models';
import Web3 from 'web3';

program
  .requiredOption('--snapshotDate <date>', 'Date of previous snapshot')
  .option(
    '--startingTime <number>',
    'Initial time in Unix timestamp from which calculating rewards',
  )
  .option('--duration <number>', 'Duration of the the snapshot to create');

program.parse(process.argv);

(async () => {
  const beforeDate = parseDate(assertIsString(program.snapshotDate));
  const durationInSeconds = assertIsFiniteNumber(
    parseInt(program.duration) || parseInt(env.spSlotDuration),
  );
  const afterDate = formatDate(addSeconds(beforeDate, durationInSeconds));
  const folder = './data/snapshots/sponsoring-pool';
  const oldSnapshotNames = await fs.readdir(folder);

  const web3 = getWeb3('mainnet');
  console.log('Starting program.');

  const snapshots: SnapshotWithName[] = (
    await createSnapshots(
      oldSnapshotNames,
      folder,
      web3,
      durationInSeconds,
      afterDate,
    )
  ).filter(snapshot => snapshot != undefined);

  const totalDeposited = calculateTotalDeposited(snapshots);
  console.log(
    '---------\ntotal deposited:',
    replaceBN(totalDeposited),
    '\n--------------',
  );
  const aggregateSnapshot = calculateAggregate(snapshots);
  await writeJsonToFile(
    `${folder}/Aggregate-${afterDate}.json`,
    aggregateSnapshot,
  );
  console.log('Calculation of rewards for SponsoringPool token');
  const reward = calculateTotalReward(
    parseDateTime(assertIsString(program.snapshotDate)),
    durationInSeconds,
    'sponsoring_pool',
  );
  if (reward === null) {
    return;
  }

  const recipients = calculateUserReward(
    aggregateSnapshot.participants,
    reward,
  );
  const sortedRecipients = groupByAddressAndSortByAmount(recipients);
  console.log(`Creating new reward snapshot for SponsoringPool`);
  await saveRecipientsFile(
    sortedRecipients.array,
    `./data/token_distributions/sponsoring-pool/${afterDate}.json`,
  );
  console.log('Program execution complete.');
})();

async function createSnapshots(
  snapshotNames: string[],
  folder: string,
  web3: Web3,
  durationInSeconds: number,
  afterDate: string,
): Promise<SnapshotWithName[]> {
  return await Promise.all(
    Object.entries(env.AaveTokens).map(async ([tokenName, feature]) => {
      const snapshotName = snapshotNames.filter(function (snapshot: string) {
        return (
          snapshot.startsWith(tokenName + '-' + program.snapshotDate) &&
          snapshot.endsWith('.json')
        );
      });
      const previousSnapshot = (await readJsonFromFile(
        `${folder}/${snapshotName}`,
      )) as Snapshot;
      const Atoken = await getContract<AToken>(feature.address, web3, {
        type: 'build-artifact',
        contractName: 'AToken',
      });
      const startTimestamp =
        previousSnapshot.endingTime || parseInt(program.startingTime);
      console.log(
        `Starting calculation of SponsoringPool ${tokenName} weights`,
      );
      const decimals = feature.decimals;
      const snapshot = await AaveCalculate({
        web3,
        Atoken,
        tokenName,
        previousSnapshot,
        startTimestamp,
        duration: durationInSeconds,
        decimals,
      });
      console.log(
        `Creating new weight snapshot for SponsoringPool token ${tokenName}`,
      );

      await writeJsonToFile(
        `${folder}/${tokenName}-${afterDate}.json`,
        snapshot,
      );
      if (Object.keys(snapshot.participants).length === 0) {
        console.log(`No users in SponsoringPool with token ${tokenName}`);
        return;
      }
      return snapshot;
    }),
  );
}
