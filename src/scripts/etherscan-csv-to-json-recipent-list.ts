require('./runtime_config');
import program from 'commander';
import { parseCsvRecipientList } from '../base/csv';
import { recipientListFromJson, saveRecipientsFile } from '../base/models';
import { assertIsBoolean, assertIsString } from '../base/utils';

program
  .requiredOption(
    '-i, --input <path>',
    'Path to input CSV file to convert to JSON',
  )
  .requiredOption(
    '-o, --output <path>',
    'Output path for the JSON file',
  )
  .option(
    '--amount-in-wei',
    'Whether to',
    false
  )
  .parse(process.argv);

(async () => {
  const input = assertIsString(program.input);
  const output = assertIsString(program.output);
  const amountInWei = assertIsBoolean(program.amountInWei);
  console.log(`Converting '${input}' to '${output}'...`);
  const result = recipientListFromJson(parseCsvRecipientList(input));
  await saveRecipientsFile(result, output, amountInWei);
  console.log(`Total records parsed: ${result.length}`);
})();
