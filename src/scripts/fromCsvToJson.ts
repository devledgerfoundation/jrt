import program from 'commander';
import BN from 'bn.js';
const fs = require('fs');
import { env } from '../base/config';
import type { Snapshot } from '../calculations/snapshot';
import { formatDateTime } from '../base/formatting';
import { zero } from '../base/utils';

program
  .requiredOption('--snapshotDate <string>', 'Date of snapshots to convert')
  .requiredOption('--initBlock <number>', 'Initial block of snapshot')
  .requiredOption('--endBlock <number>', 'Ending block of snapshot')
  .requiredOption('--initTime <number>', 'Initial timestamp of snapshot')
  .requiredOption('--endTime <number>', 'Ending timestamp of snapshot')
  .option('--SponsoringPool', 'Creation of Json for SponsoringPool', false);

program.parse(process.argv);

type User = {
  // mapping of address to balance for each participant
  [address: string]: {
    balance: BN;
    weight: BN;
    timestampOfLastTransfer?: number;
    updated?: boolean;
  };
};
(async () => {
  console.log('Starting conversion from CSV to JSON format.');

  if (program.SponsoringPool) {
    const folder = './data/snapshots/sponsoring-pool';
    const snapshotNames = fs.readdirSync(folder);
    await Promise.all(
      Object.entries(env.AaveTokens).map(async ([token]) => {
        console.log(snapshotNames);
        const snapshotName: string = snapshotNames.filter(function (
          snapshot: string,
        ) {
          return (
            snapshot.startsWith(token + '-' + program.snapshotDate) &&
            snapshot.endsWith('.csv')
          );
        })[0];
        console.log(snapshotName);
        const snapshot = fs.readFileSync(`${folder}/${snapshotName}`, 'utf8');
        let lines = snapshot.split('\n');
        const headers = lines[0].split(',');
        let holders: { address: string; balance: string }[] = [];
        for (var i = 1; i < lines.length - 1; i++) {
          const currentLine = lines[i].split(',');
          holders.push({ address: currentLine[0], balance: currentLine[1] });
        }
        const userPositions = Object.fromEntries(
          holders.map(({ address, balance }) => [
            address,
            { balance: new BN(balance), weight: zero },
          ]),
        );
        const newSnapShot: Snapshot = {
          startBlock: parseInt(program.initBlock),
          endBlock: parseInt(program.endBlock),
          startingTime: parseInt(program.initTime),
          endingTime: parseInt(program.endTime),
          participants: userPositions,
        };
        const outputPath = `${folder}/${snapshotName.replace('.csv', '.json')}`;
        console.log('generating file' + outputPath);
        fs.writeFileSync(outputPath, JSON.stringify(newSnapShot));
      }),
    );
  } else {
    const snpashotsNames = fs.readdirSync('./data/snapshots');
    await Promise.all(
      Object.entries(env.ethAddresses.liquidityPools).map(async ([pool]) => {
        const snapshotDate = program.snapshotDate;
        const snapshotName = snpashotsNames.filter(function (snapshot: string) {
          return snapshot.startsWith(`${pool}-${snapshotDate}`);
        });
        if (!snapshotName.length) {
          console.warn(
            `Snapshot for pool '${pool} not found with date ${snapshotDate}' - skipping.`,
          );
          return;
        }
        console.log(`Attempting to read snapshot with name: '${snapshotName}'`);
        const snapshot = fs.readFileSync(
          './data/snapshots/' + snapshotName,
          'utf8',
        );
        let lines = snapshot.split('\n');
        const headers = lines[0].split(',');
        let holders: { address: string; balance: string }[] = [];
        for (var i = 1; i < lines.length - 1; i++) {
          const currentline = lines[i].split(',');
          holders.push({ address: currentline[0], balance: currentline[1] });
        }
        const userPositions = Object.fromEntries(
          holders.map(({ address, balance }) => [
            address,
            {
              balance: new BN(balance),
              weight: zero,
              timestampOfLastTransfer: 0,
              updated: false,
            },
          ]),
        );
        const newSnapShot: Snapshot = {
          startBlock: parseInt(program.initBlock),
          endBlock: parseInt(program.endBlock),
          startingTime: parseInt(program.initTime),
          endingTime: parseInt(program.endTime),
          participants: userPositions,
        };
        const outputPath =
          './data/snapshots/' +
          pool +
          '-' +
          formatDateTime(new Date()) +
          '.json';
        console.log('generating file' + outputPath);
        fs.writeFileSync(outputPath, JSON.stringify(newSnapShot));
      }),
    );
  }
})();
